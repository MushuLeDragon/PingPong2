#!/bin/bash

sleep 30
echo "Test du serveur : $URL"

count=$(curl -s $URL/count | ./jq-linux64 -r '.pingCount')
echo "Attendu : 0 - Obtenu : $count"
if [ $count -eq 0 ]
then
  echo "Count fonctionne"
else
  echo "Count ne fonctionne pas"
  exit 1
fi

piing=$(curl -s $URL/ping | ./jq-linux64 -r '.message')
echo "Attendu : pong - Obtenu : $piing"
if [ $piing=="pong" ]
then
  echo "Ping fonctionne"
else
  echo "Ping ne fonctionne pas"
  exit 1
fi

pingcount=$(curl -s $URL/count | ./jq-linux64 -r '.pingCount')
echo "Attendu : 1 - Obtenu : $pingcount"
if [ $pingcount -eq 1 ]
then
  echo "L'incrémentation se fait"
else
  echo "Pas d'incrémentation"
  exit 1
fi

echo "L'application fonctionne correctement."
exit 0
