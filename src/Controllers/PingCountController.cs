﻿using Microsoft.AspNetCore.Mvc;

namespace PingCount.Controllers
{
    [Route("")]
    public class PingCountController : Controller
    {
        private static int count = 0;

        // GET ping
        [HttpGet("ping")]
        public JsonResult Ping()
        {
            var response = new PingResponse()
            {
                Message = "pong",
            };

            count++;

            return Json(response);
        }

        // GET count
        [HttpGet("count")]
        public JsonResult Count(int id)
        {
            var response = new CountResponse()
            {
                PingCount = count,
            };

            return Json(response);
        }
    }
}
