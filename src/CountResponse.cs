﻿namespace PingCount
{
    public class CountResponse
    {
        public int PingCount { get; set; }
    }
}